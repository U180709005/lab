public class MyDateTime {

    MyDate date;
    MyTime time;

    public MyDateTime(MyDate date, MyTime time) {
        this.date=date;
        this.time=time;
    }

    public void incrementDay() {
        date.incrementDay();
    }

    public void incrementDay(int diff) {
        date.incrementDay(diff);
    }

    public void incrementHour() {
        time.incrementHour();
    }

    public void incrementHour(int diff) {
        int dayDiff = time.incrementHour(diff);
        if (dayDiff < 0) {
            date.decrementDay(-dayDiff);
        } else {
            date.incrementDay(dayDiff);
        }
    }

    public void decrementHour(int diff) {
        incrementHour(-diff);
    }

    public void incrementMinute(int diff) {
        int dayDiff = time.incrementMinute(diff);
        if (dayDiff < 0) {
            date.decrementDay(-dayDiff);
        } else {
            date.incrementDay(dayDiff);
        }
    }

    public void decrementMinute(int diff) {
        time.decrementMinute(diff);
    }

    public void incrementYear(int diff) {
        date.incrementYear(diff);
    }

    public void decrementDay() {
        date.decrementDay();
    }

    public void decrementDay(int diff) {
        date.decrementDay(diff);
    }

    public void decrementYear() {
        date.decrementYear();
    }

    public void decrementYear(int diff) {
        date.decrementYear(diff);
    }

    public void decrementMonth() {
        date.decrementMonth();
    }

    public void decrementMonth(int diff) {
        date.decrementMonth(diff);
    }

    public void incrementMonth(int diff) {
        date.incrementMonth(diff);
    }

    public void incrementMonth() {
        date.incrementMonth();
    }

    public void incrementYear() {
        date.incrementYear();
    }

    public boolean isBefore(MyDateTime anotherDateTime) {
        if (date.isBefore(anotherDateTime.date)){
            return true;
        }else if(date.isAfter(anotherDateTime.date)){
            return false;
        }
        if (time.isBefore(anotherDateTime.time)){
            return true;
        }
        return false;
    }

    public boolean isAfter(MyDateTime anotherDateTime) {
        return !isBefore(anotherDateTime);
    }

    public String dayTimeDifference(MyDateTime anotherDateTime) {
        int dayDiff;
        int minDiff;
        int hourDiff;

        if(isBefore(anotherDateTime)) {
            dayDiff = date.dayDifference(anotherDateTime.date);
            minDiff = anotherDateTime.time.minute - time.minute;
            hourDiff = anotherDateTime.time.hour - time.hour;
        }else {
            dayDiff = anotherDateTime.date.dayDifference(date);
            minDiff = time.minute - anotherDateTime.time.minute;
            hourDiff = time.hour - anotherDateTime.time.hour;
        }

        if (minDiff < 0) {
            minDiff += 60;
            hourDiff -= 1;
        }

        if (hourDiff < 0) {
            hourDiff += 24;
            dayDiff -= 1;
        }

        String output = new String();
        if (dayDiff != 0) {
            output += dayDiff + " day(s) ";
        }
        if (hourDiff != 0) {
            output += hourDiff + " hour(s) ";
        }
        if (minDiff != 0) {
            output += minDiff + " minute(s) ";
        }
        return output;
    }


    public String toString(){
        return date.toString() + " " + time.toString();
    }
}
