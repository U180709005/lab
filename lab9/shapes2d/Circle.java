package shapes2d;

public class Circle {
    protected double radius;

    public Circle(double radius){
        this.radius=radius;
    }

    @Override
    public String toString() {
        return "Circle Radius = " + radius +
                ", Circle Area = " + area();
    }

    public double area(){
        return Math.PI*Math.pow(radius,2);
    }
}
