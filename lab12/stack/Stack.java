package stack;

public interface Stack {
    void push(Object Item);
    Object pop();
    boolean empty();
}
