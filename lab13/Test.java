public class Test {

    public static void main(String[] args) throws NumberConvertException {
        StringToIntConverter converter = new StringToIntConverter();


        System.out.println(converter.convert("five"));
        System.out.println(converter.convert("nine"));
        System.out.println(converter.convert("three"));

        try {
            int value = converter.convert("hello");
        }catch (NumberConvertException e){
            System.out.println("Your input is not valid!!!");
        }

        try {
            int a = 8/0;
        }catch (ArithmeticException e){
            System.out.println("Enter a non-zero divisor");
        }
    }
}
