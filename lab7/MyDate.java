public class MyDate {

    private int day;
    private int month;
    private int year;

    int[] maxDays = {31,28,31,30,31,30,31,31,30,31,30,31};
    int maxMonth = 11;

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month-1;
        this.year = year;
    }

    public void incrementDay(int addDay) {
        int maxDay=maxDays[month];
        int newDay=(day+addDay)%(maxDay+1);
        if (newDay==0){
            newDay=newDay+1;
        }
        int addMonth=((day+addDay)/(maxDay+1));
        incrementMonth(addMonth);

        day=newDay;
    }

    public void incrementMonth(int addMonth) {
        int newMonth=(month+addMonth)%12;
        int addYear=((month+addMonth)/12);
        incrementYear(addYear);

        boolean check1= false;
        int oldMonthDay=0;
        if(maxDays[month]==day){
            check1=true;
            oldMonthDay=maxDays[month];
        }

        month=newMonth;

        if(check1&&(oldMonthDay>maxDays[month])){
            day=maxDays[month];
        }
        if(leapYear()&&(month==1)){
            day+=1;
        }

    }

    public void incrementYear(int addYear) {
        boolean check1=false;
        boolean check2=false;

        if(leapYear()) check1 = true;

        year=year+addYear;

        if(leapYear()) check2 = true;

        if(check1 && !check2 && (month==1)){
            day=(day-1);
        }
    }


    public void decrementDay(int subDay) {
        int newDay=day-subDay;
        int subMonth = 0;
        while(newDay<=0){
            subMonth=subMonth+1;
            if(leapYear()){
                newDay=29;
            }else {
                newDay = newDay + maxDays[month - subMonth];
            }
        }
        day=newDay;
        decrementMonth(subMonth);

    }

    public void decrementMonth(int subMonth) {
        int newMonth=month-subMonth;
        int subYear = 0;

        boolean check1= false;
        int oldMonthDay=0;
        if(maxDays[month]==day){
            check1=true;
            oldMonthDay=maxDays[month];
        }
        while(newMonth<0){
            newMonth=newMonth+12;
            subYear=subYear+1;
        }

        month=newMonth;
        decrementYear(subYear);

        if(check1&&(oldMonthDay>maxDays[month])){
            day=maxDays[month];
        }
    }

    public void decrementYear(int subYear) {

        boolean check1=false;
        boolean check2=false;

        if(leapYear()) check1 = true;

        year=year-subYear;

        if(leapYear()) check2 = true;

        if(check1 && !check2 && (month==1)){
            day=(day-1);
        }
    }


    public void decrementDay() {
        decrementDay(1);
    }

    public void decrementMonth() {
        decrementMonth(1);
    }

    public void decrementYear() {
        decrementYear(1);
    }


    public void incrementDay() {
        incrementDay(1);
    }

    private boolean leapYear(){
        return(year%4==0);
    }


    public void incrementMonth() {
        incrementMonth(1);
    }

    public void incrementYear() {
        incrementYear(1);
    }


    public boolean isBefore(MyDate anotherDate) {
        return !isAfter(anotherDate);
    }

    public boolean isAfter(MyDate anotherDate) {
        if(year<anotherDate.year) {
            return false;
        }else if(year==anotherDate.year) {
            if(month<anotherDate.month){
                return false;
            }else if(month==anotherDate.month){
                if(day<= anotherDate.day){
                    return false;
                }
            }
        }

        return true;
    }

    public int dayDifference(MyDate anotherDate) {
        if (isAfter(anotherDate)) {
            int leftDays = maxDays[anotherDate.month] - anotherDate.day;
            int currentMonth = anotherDate.month;
            while (month > currentMonth + 1) {
                currentMonth += 1;
                leftDays += maxDays[currentMonth];
            }
            if (year == anotherDate.year && month == anotherDate.month) {
                return day - anotherDate.day;
            }
            leftDays += day;
            return leftDays;
        }else{
            int leftDays = maxDays[month] - day;
            int currentMonth = month;
            while (anotherDate.month > currentMonth + 1) {
                currentMonth += 1;
                leftDays += maxDays[currentMonth];
            }
            if (anotherDate.year == year && anotherDate.month == month) {
                return anotherDate.day - day;
            }
            leftDays += anotherDate.day;
            return leftDays;
        }
    }

    public String toString(){
        return year + "-" + ( (month+1) < 10? "0" : "" ) + (month+1) +"-" + ( day < 10? "0" : "" )  + day;
    }

}