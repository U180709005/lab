import static java.lang.Math.sqrt;

public class FindPrimes {
    public static void main(String[] args) {

        int limit = Integer.parseInt(args[0]);

        int counter = 0;

        for(int number = 2; number < limit; number++){
            boolean isPrime = true;

            int divisor = 2;

            while(divisor <= Math.sqrt(number) &&  isPrime) {
                if (number % divisor == 0) {
                    isPrime = false;
                    break;
                }
                divisor++;
            }

            if(isPrime && counter==0){
                System.out.print(number);
                counter++;
            }else if(isPrime){
                System.out.print("," + number);
            }

        }
    }
}